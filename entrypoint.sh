#!/bin/bash

sed -i "s/# DBPassword=/DBPassword=password/g" /etc/zabbix/zabbix_server.conf
sed -i "s/#/ /g" /etc/zabbix/nginx.conf
sed -i "s/example.com/localhost/g" /etc/zabbix/nginx.conf
sed -i "s/; //g" /etc/zabbix/php-fpm.conf
sed -i "s/Riga/Kiev/g" /etc/zabbix/php-fpm.conf

service mysql start
mysql -e"create database zabbix character set utf8 collate utf8_bin;"
mysql -e"create user zabbix@localhost identified by 'password';"
mysql -e"grant all privileges on zabbix.* to zabbix@localhost;"
mysql -e"FLUSH PRIVILEGES;"
zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql --database=zabbix
service zabbix-server start
service zabbix-agent start
service php7.2-fpm start
nginx -g "daemon off;"

service nginx reload
