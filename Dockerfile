FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Kiev

RUN echo "" > /etc/dpkg/dpkg.cfg.d/excludes
RUN mkdir -p /etc/mysql/conf.d/ &&\
    echo "[client]" > /etc/mysql/conf.d/my.cnf &&\
    echo "user=root" >> /etc/mysql/conf.d/my.cnf &&\
    echo "password=password" >> /etc/mysql/conf.d/my.cnf

RUN apt update && apt install wget mysql-server -y && \
    wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+bionic_all.deb &&\
    dpkg -i zabbix-release_5.0-1+bionic_all.deb &&\
    apt update && apt upgrade -y && apt install -y zabbix-server-mysql zabbix-frontend-php zabbix-nginx-conf zabbix-agent

ADD entrypoint.sh /entrypoint.sh

EXPOSE 80

ENTRYPOINT ["bash", "/entrypoint.sh"]
